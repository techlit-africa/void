# TechLit Void Linux

This code is installed on all TechLit systems. It is used to bootstrap and update all system components.

WARNING: this repo is changing rapidly, so by default, you should ask Tyler

## Prerequisites

The only requirement for this code is that it runs on a void linux machine.

If you'd like to use another system, and you set up virtualization, please share instructions here.

## Getting Started

First, clone the code onto your void machine (if you're on a TechLit machine, then you probably already have
the code at `/srv/techlit`).

After cloning, use the `tl-repo-fetch` script to set up the components that you're interested in.

```
git clone git@gitlab.com:techlit-africa/techlit-void.git

cd techlit-void

./bin/tl-repo-fetch
```

## Maintained Commands

```
tl-repo-fetch
tl-repo-pull
tl-repo-push
tl-repo-status

tl-provision-desktop
tl-provision-installer
tl-provision-mirror

tl-write-desktop
tl-write-installer
tl-write-recovery

tl-bless-desktop
tl-bless-installer
tl-bless-mirror

tl-bootstrap-desktop
tl-bootstrap-live
tl-bootstrap-mirror

tl-install-shared-all
tl-install-shared-live
tl-install-shared-desktop
tl-install-shared-hardware
tl-install-desktop-client
tl-install-desktop-server
tl-install-live-installer
tl-install-live-recovery
tl-install-cloud-mirror

tl-bake-live
tl-bake-desktop
```
