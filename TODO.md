

  1. [x] Scripts to bootstrap & initialize mirror

  1. [x] tl-sync-{init,fetch,status,pull,push}
    a. [x] {self,social,desktop,recovery,installer,theme}

  1. [] Separate system theme & bootstrap
    a. [] {grub,splash,sounds,ui,icons,cursors,fonts}

  1. [] Install basics
    a. [] intro {gcompris,tuxmath,ktuberling}
    a. [] games {supertux{2,kart},minetest}
    a. [] extras {inkscape,blender,olive}

  1. [] XBPS packages
    a. [] Rainloop
    a. [] OnlyOffice
    a. [] grub-silent
    a. [] scratch desktop
    a. [] scratch jr desktop

  1. [] faster iteration via write & bake

  1. [] Guest login
    a. [] Reset subvol
    a. [] Replace name
    a. [] Script to replace snapshot

  1. [] Cinnamon
    a. [] Menu
    a. [] Keybinds

  1. [] Social
    a. [] finding server (mdns)
    a. [] ssh commands (mdns)
    a. [] list clients with stats & commands

  1. [] ngircd + hexchat (mdns)

  1. [] dovecot + rainloop (mdns)

  1. [] server/client switching

  1. [] Add dracut splash module (like plymouth)

  1. [] Early kernel logging ... compile without debug?

  1. [] Create tl-erp-* scripts with token caching

  1. [] AutoSSH service for servers

  1. [] Snooze
    a. [] Mirror: every 12h remove tl-erp token (busts cache)
    a. [] Server: every 1d check for Desktop updates
    a. [] Server: every 5m status to erp
    a. [] Client: every 5m status to server
    a. [] Desktop: every 1d btrfs fstrim

  1. [] Net::SSH brute force from erp
