#!/usr/bin/env bash

# Get absolute repository root (especially when symlinked)
ROOT="$(realpath "$(dirname "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")")"

# Load accompanying bash library
source "$ROOT/lib/lib.bash"

# Exit now if run as root user
assert-is-user

DISK="$1"
HOSTNAME="$2"
if [ -z "$DISK" ] || [ -z "$HOSTNAME" ]; then
  say "USAGE:  $(g "${BASH_SOURCE##*/}")  $(c /dev/sdx)  $(c tli-000)"
  exit 1
fi

say "This script will need root access via $(c sudo)"
sudo echo thank you

# Mountpoint to use
PREFIX="${PREFIX:-"$(mktemp -d)"}"

say "Purging USB partition table"
run sudo dd if="/dev/zero" of="$DISK" status=progress bs=4M count=200

say "Creating new partition table"; {
  run sudo parted "$DISK" -s mktable gpt || true
  run sudo parted "$DISK" -s mkpart primary 0 10M || true
  run sudo parted "$DISK" -s mkpart primary fat32 10M 522M || true
  run sudo parted "$DISK" -s mkpart primary 522M 1500M || true
  run sudo parted "$DISK" -s mkpart primary ext4 1500M 100% || true
  run sudo parted "$DISK" -s set 1 bios_grub on || true
  run sudo parted "$DISK" -s set 2 esp on || true
}

say "Waiting for kernel to recognize new partitions"; {
  part_name=$(echo "$DISK"2 | sed -e 's;/dev/;;g')
  while ! sudo lsblk | grep -q "$part_name"; do
    run sudo partprobe "$DISK"
    run sleep 0.5
  done
}

say "Creating filesystems"; {
  run sudo mkfs.fat -F32 "${DISK}2"
  run sudo mke2fs -t ext4 -F "${DISK}4"
}

say "Mounting root filesystem (and un-mounting on errors or exit)"; {
  cleanup() {
    say "Un-mounting root filesystem"
    if sudo grep -q "$PREFIX" "/etc/mtab"; then
      run sudo umount -R "$PREFIX"
    fi
  }

  run sudo mount "${DISK}4" "$PREFIX"
  run sudo rm -df "$PREFIX/lost+found"
}

say "Initializing repo for updates"
run sudo -E "$ROOT/bin/tl-repo-fetch" -ab -LS -d "$PREFIX/techlit-void"

say "Configuring $(c secure)"; {
  run sudo chmod 700 "$PREFIX/secure"
  run sudo mkdir -p -m 700 "$PREFIX/secure/ssh"
  run sudo mkdir -p -m 700 "$PREFIX/secure/net/system-connections"
  run sudo mkdir -p -m 700 "$PREFIX/secure/net/lib"
  run sudo touch "$PREFIX/secure/known_hosts"
  run sudo touch "$PREFIX/secure/bash_history"
}

say "Creating SSH identity"; {
  dir="$PREFIX/secure/ssh"
  for algo in ecdsa ed25519 rsa; do
    key="$dir/ssh_host_${algo}_key"
    if [ -f "$key" ]; then
      say " >> Destroying existing keys: $(c "$key{,.pub}")"
      run sudo rm "$key"{,.pub}
    fi
    run sudo ssh-keygen -f "$key" -N "" -t "$algo" -C "admin@$HOSTNAME"
  done
}

say "Creating $(c rc.conf) file for USB"; {
  run root-write "$PREFIX/secure/rc.conf" "
#
# Void configuration
#
export HOSTNAME=\"$HOSTNAME\"
export TIMEZONE=\"$TIMEZONE\"
export TZ=\"\$TIMEZONE\"
export KEYMAP=\"$KEYMAP\"
export LOCALE=\"$LOCALE\"
export HARDWARECLOCK=\"UTC\"
export TTYS=4
export CGROUP_MODE=unified
export SEEDRING_SKIP_CREDIT=true

#
# TechLit versions
#
export TL_ROLE=\"recovery\"
export TL_PORT=\"\"
export TL_SERVER=\"\"
export TL_IDENTITY=\"$(sudo cat "$key.pub")\"
export TL_ACCESS=\"
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDvjPrKNZFEpMTcbcLdlESKSfG5DqT4inbxIhdLYb9CBm141y3L7UKMXgMaVirdRzU1YEebzSAyTfvuvsmIUumrM4olD4x+0cEj1Y5B8auCISprvP3auslVhqlITwLkhIu2HbraDLMS8HbYH+fYQyuYqQuKJQ8RtJrBkjOgUmnKuUSenFOZtpGIK6MmEkYbC9/mYxJz2gb+uKNRfF/YdGy1yb+r4fhxowXsP/AQTW5OCHyZPqaetTr220tCCbb/lxlbRETkOE0UF9X1Rtyp250YB1aY2kDIP7YNdJDI5eqK6ClXWUjO7wWu4ydQ+cIOVAy6Rh2GQkmML7hFbVcEZ1c52JdDgBayUp1R/o4mJ+TOAVgAaVYzzEad+j4GKrNreKr+wcaWJS3OE3y3krqp7mNaEW2/pk3Dv94gipBw7vbdoXxrI2vf4GZ/QeHaMtTRb8xEYZWgr0dS+m0R8ILQUoWt7C9WZO0yTK3ikK1ZoY5U40WNht2XFJrj0OpB7+Mut5U= tyler@macvoid
\"
"
}

say "Changing ownership"
run sudo chown -R "$ADMIN_UID:$ADMIN_GID" "$PREFIX"

# Cleanup and quiet exit callback
cleanup; cleanup() { :; }
say "Done."
