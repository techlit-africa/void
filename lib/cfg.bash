# Repo root; code & updates location
DEFAULT_ROOT="/srv/techlit-void"

# Remote URLs
MIRROR_URL="void.techlitafrica.org"
GITLAB_URL="gitlab.com"

ROOTFS_URL="https://repo-default.voidlinux.org/live/current/void-x86_64-ROOTFS-20221001.tar.xz"

#
# Locale settings
#
KEYMAP=us
LOCALE=en_US.UTF-8
TIMEZONE=Africa/Nairobi

#
# dev: privileged SSH user
#
DEV_USER=dev
DEV_UID=500
DEV_GROUP=dev
DEV_GID=500

#
# admin: unprivileged SSH user
#
ADMIN_USER=admin
ADMIN_UID=1000
ADMIN_GROUP=admin
ADMIN_GID=1000
ADMIN_PASS=empowerwatoto

#
# guest: unprivileged GUI user
#
GUEST_USER=guest
GUEST_UID=2000
GUEST_GROUP=guest
GUEST_GID=2000
