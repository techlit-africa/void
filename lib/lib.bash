#
# DO NOT LOAD INTERACTIVELY
#
if [[ $- =~ i ]]; then
  echo "${BASH_SOURCE[0]} is not meant for interactive use" >&2
  echo "Refusing to continue" >&2
  return 1
fi

#
# Load configuration first
#
source "$(realpath "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")/cfg.bash"

#
# Shell options (see bash manual for more info)
#
# -e          # Exit on first error
# -E          # ERR traps called from functions too
# -T          # DEBUG and RETURN traps called from functions too
# -o pipefail # Debug
#
set -eETo pipefail

#
# When using getopts, print errors
#
OPTERR=1

#
# Color shortcuts
#
# r=red, g=green, y=yellow, c=cyan, x=reset
#
# Print the variable version to change to a color
#
# Call the function version to print in a color,
#   then change back to yellow
#
TERM="${TERM:-xterm}"
r="$(echo -e '\e[1;31m')"; r() { echo -e "$r$*$y"; }
g="$(echo -e '\e[1;32m')"; g() { echo -e "$g$*$y"; }
y="$(echo -e '\e[1;33m')"; y() { echo -e "$y$*$y"; }
c="$(echo -e '\e[1;36m')"; c() { echo -e "$c$*$y"; }
x="$(echo -e '\e[0m')"

#
# Logging helpers
#
# say prints a colorful, indented comment
say() { echo -e "$*" | sed -e "s/^/$y# => /g" | sed -e "s/$/$x/g" >&2; }
# err prints a colorful, indented error
err() { echo -e "$*" | sed -e "s/^/$r# !! /g" | sed -e "s/$/$x/g" >&2; }
# run prints a colorful, indented command, then runs it
run() { echo -e "$y# $c$ $g$*$x" >&2; "$@"; }

#
# Cleanup helpers
#
# Run a cleanup function before exiting
trap 'cleanup; cleanup() { :; }' EXIT ERR HUP INT QUIT TERM
# Redefine cleanup wherever to override exit behavior
cleanup() { :; }

#
# Chroot helpers
#
# NOTE: Before calling the functions, you need to set:
#       - PREFIX # the target root directory

# USAGE: tl-chroot "root:wheel" "x && y 2>&1"
tl-chroot() { chroot --userspec "$1" "${PREFIX:?}" "/bin/bash" -c "$2"; }
# USAGE: root-chroot "x && y 2>&1"
root-chroot() { tl-chroot "root:root" "$1"; }
# USAGE: admin-chroot ". ~/.bashrc"
admin-chroot() { tl-chroot "admin:admin" "$1"; }

# USAGE: tl-chroot-mount
tl-chroot-mount() {
  # Required mounts for chroot
  for dir in sys dev proc; do
    run mkdir -p "$PREFIX/$dir"
    run mount --rbind "/$dir" "$PREFIX/$dir"
  done

  # Simplest possible resolvconf
  run mkdir -p "$PREFIX/etc"
  run write "$PREFIX/etc/resolv.conf" "nameserver 8.8.8.8"
}

# USAGE: tl-chroot-unmount
tl-chroot-unmount() {
  run rm -f "$PREFIX/etc/resolv.conf"
  for dir in sys dev proc; do
    run umount -qR "$PREFIX/$dir"
  done
}


#
# Filesystem helpers
#
# These are useful in combination when logging with the run helper

# USAGE: run write "rc.conf" "x = y"
write() { echo "$2" > "$1"; }

# USAGE: run append "rc.conf" "x = y"
append() { echo "$2" >> "$1"; }

# USAGE: run root-write "rc.conf" x = y"
root-write() {
  local t="$(mktemp)"
  write "$t" "$2" && sudo cp "$t" "$1"
}

# USAGE: run root-append "rc.conf" x = y"
root-append() {
  local t="$(mktemp)"
  append "$t" "$2" && sudo cp "$t" "$1"
}

#
# Install helpers
#
# NOTE: Before calling the functions, you need to set:
#       - SRC  # the source directory
#       - DEST # the destination directory

# USAGE: tl-install rc.conf 0 0 640 rc.conf
tl-install() {
  install -o "$2" -g "$3" -m "$4" -vD "$SRC/$1" "$DEST/$5"
}

# USAGE: tl-root-install rc.conf 0 0 640 rc.conf
tl-root-install() {
  sudo install -o "$2" -g "$3" -m "$4" -vD "$SRC/$1" "$DEST/$5"
}

# USAGE: tl-install-dir conf.d 0 0 640 conf.d
tl-install-dir() {
  [ -d "$DEST/$5" ] && rm -rf "${DEST:?}/$5"
  mkdir -p "$DEST/$5"

  (
    cd "$SRC/$1" || false
    find . -type f -exec install -o"$2" -g"$3" -m"$4" -vD "{}" "$DEST/$5/{}" \;
  )
}

# USAGE: tl-install-svc sva svb svc
tl-install-svc() {
  for svc in "$@"; do
    ln -svf "/etc/sv/$svc" "$DEST/etc/runit/runsvdir/default"
  done
}

#
# Bootstrap helpers
#
# NOTE: Before calling the functions, you need to set:
#       - PREFIX # the root of the system to be bootstrapped
#       - LOCALE # the initial locale for configuring packages
#
#       $PREFIX/srv/void-mklive is used to cache XBPS packages
#       $PREFIX/srv/techlit-void is used to install roles

# USAGE: tl-bootstrap-xbps pkga pkgb pkgc...
tl-bootstrap-xbps() {
  # Where to find and save the void rootfs tarball
  local rootfs="$ROOT/srv/.tl-repo/$(basename "$ROOTFS_URL")"

  local cache="$ROOT/srv/void-mklive/xbps-cachedir-x86_64"
  local tl_repo="http://void.techlitafrica.org/xbps"
  local de_repo="https://repo-de.voidlinux.org/current"

  local xbpsops=" -r $PREFIX "
  xbpsops+=" -R $tl_repo -R $tl_repo/nonfree "
  xbpsops+=" -R $de_repo -R $de_repo/nonfree "
  xbpsops+=" -c $cache "

  say "Fetching & installing a Void rootFS tarball"; {
    if [ -f "$rootfs" ]; then
      say " >> Cached: $(c "$rootfs")"
    else
      say " >> Downloading..."
      run curl "$ROOTFS_URL" -o "$rootfs"
    fi

    if [ -z "$SKIP_UNPACK" ]; then
      say " >> Unpacking..."
      run tar xf "$rootfs" -C "$PREFIX"
    fi
  }

  say "Copying XBPS keys for noninteractive install"; {
    SRC="$ROOT/roles/global/"; DEST="$PREFIX"
    run tl-install-dir "xbps-keys" 0 0 644 "var/db/xbps/keys"
  }

  say "Installing XBPS packages"; {
    run "$PREFIX/bin/xbps-install" $xbpsops -Suy xbps
    run "$PREFIX/bin/xbps-install" $xbpsops -Uuy "$@"
    run "$PREFIX/bin/xbps-install" $xbpsops -SUuy
  }

  say "Configuring XBPS packages"; {
    run root-chroot "xbps-reconfigure -f base-files"
    if [ -f "$PREFIX/etc/default/libc-locales" ]; then
      run sed -e "s/\#\($LOCALE.*\)/\1/g" -i "$PREFIX/etc/default/libc-locales"
    fi
    run root-chroot "xbps-reconfigure -a"
  }

  say "Removing unused directories"
  run rm -rf "$PREFIX/srv/www"
}

# USAGE: tl-bootstrap-roles /srv/techlit-void rolea roleb...
tl-bootstrap-roles() {
  say "Installing roles:"
  for role in "$@"; do
    say "  $(c "$role")"
    run "$ROOT/roles/$role/install" "$PREFIX"
  done
}

# USAGE: tl-bootstrap-kernel
tl-bootstrap-kernel() {
  vmlinuz="$(find "$PREFIX/boot/" -name "vmlinuz*" | sort | tail -n1)"
  initrd="$(find "$PREFIX/boot/" -name "initramfs*" | sort | tail -n1)"

  say "Linking latest kernel and initramfs"
  run ln -srf "$vmlinuz" "$PREFIX/boot/vmlinuz"
  run ln -srf "$initrd" "$PREFIX/boot/initrd"
}

#
# User helpers
#
assert-is-user() {
  if [ -n "$TL_SKIP_USER_CHECK" ]; then
    return
  fi

  if [ "$(id -u)" = 0 ]; then
    err "This script is meant to be run as a regular user. Refusing to continue"
    say "If you know what you're doing, you can set $(c SKIP_USER_CHECK)"
    exit 1
  fi
}

assert-is-root() {
  if [ -n "$TL_SKIP_USER_CHECK" ]; then
    return
  fi

  if ! [ "$(id -u)" = 0 ]; then
    err "This script is meant to be run as ${c}root${r}. Refusing to continue"
    say "If you know what you're doing, you can set $(c SKIP_USER_CHECK)"
    exit 1
  fi
}

#
# Repo helpers
#
assert-fetched() {
  for comp in "${@}"; do
    if ! [ -d "$ROOT/srv/$comp" ] && ! [ -f "$ROOT/srv/.tl-repo/$comp.local" ]; then
      err "$c$comp$r is required. Refusing to continue"
      say "HINT: Run $(g tl-repo-fetch) $(c "$comp")"
      exit 1
    fi
  done
}
