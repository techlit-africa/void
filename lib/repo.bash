source "$(realpath "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")/lib.bash"

# Reuse SSH connections (for same user %r and host %h and process $$)
SSHOPS=""
SSHOPS+=" -o ControlPath=/tmp/.tl-repo-ssh-%r-%h "
SSHOPS+=" -o ControlMaster=auto "
SSHOPS+=" -o ControlPersist=yes "
SSHOPS+=" -o AddKeysToAgent=yes "

# Use same SSH connections for git where possible
export GIT_SSH_COMMAND="ssh $SSHOPS"

repo-refetch() { "$REPO_DIR/bin/tl-repo-fetch" "$ORIGINAL_ARGS"; }

# USAGE: repo-setup "Fetches update information and handles initial setup"
repo-setup() {
  ORIGINAL_ARGS="$@"

  # Script config defaults
  GITS=()
  NULLS=()
  RSYNCS=()
  LOCAL=
  MODE="developer"
  REPO_DIR="$ROOT"

  # Parse script options
  while getopts "d:abchLSXD" opt; do
    case "$opt" in
      a) RSYNCS+=(content desktop recovery) GITS+=(techlit-void);;
      b) NULLS+=(public secure);;
      c) RSYNCS+=(theme); GITS+=(void-mklive void-packages);;

      d) REPO_DIR="$(realpath "$(readlink -f "$OPTARG")")";;

      L) LOCAL=1;;

      S) MODE="system";;
      X) MODE="mirror";;
      D) MODE="developer";;

      *) HELP=1;;
    esac
  done
  shift $((OPTIND - 1))

  # Parse script arguments
  while [ -n "$1" ]; do
    case "$1" in
      secure) NULLS+=("secure");;
      public) NULLS+=("public");;

      theme)    RSYNCS+=("theme");;
      content)  RSYNCS+=("content");;
      desktop)  RSYNCS+=("desktop");;
      recovery) RSYNCS+=("recovery");;

      techlit-void)  GITS+=("techlit-void");;
      void-mklive)   GITS+=("void-mklive");;
      void-packages) GITS+=("void-packages");;

      *) err "Unknown component: $(c "$1")"; HELP=1; break;;
    esac

    shift
  done

  # Script config derivatives
  SRV="$(dirname "$REPO_DIR")"
  TL_SRV="$REPO_DIR/srv"

  # Componets are required, so the lists should not be empty
  if ! ((${#RSYNCS[@]} + ${#GITS[@]} + ${#NULLS[@]})); then
    HELP=1
  fi

  if [ -n "$HELP" ]; then
    say ""
    say "Usage: $(g "${0##*/}")  $(c "[-dsrpabchX]  [<comp> ...]")"
    say ""
    say "$DESCRIPTION"
    say ""
    say "Options:"
    say "  $(c "-h")        # Print this help"
    say ""
    say "  $(c "")          # $(c "Component shortcuts") -- choose any amount"
    say "  $(c "-a")        # Fetch all system update components"
    say "  $(c "-b")        # Fetch all provisional components"
    say "  $(c "-c")        # Fetch all development components"
    say ""
    say "  $(c "")          # $(c "Install modes") (only used on initial fetch)"
    say "  $(c "-D")        # Developer mode (for creating and pushing updates)"
    say "  $(c "-S")        # System mode (for creating desktops & recovery isos)"
    say "  $(c "-X")        # Mirror mode (for mirror setup -- don't want this)"
    say "  $(c "")          # (default: $(c "-D"))"
    say ""
    say "  $(c "-L")        # Local mode (for provisioning while offline)"
    say ""
    say "  $(c "-d <dir>")  # Repo root directory"
    say "  $(c "")          # (default: $(c "$ROOT"))"
    say ""
    say "System update components:"
    say "  $(c content)  # Classroom content updates"
    say "  $(c desktop)  # Desktop BTRFS updates"
    say "  $(c recovery) # Recovery ISO images"
    say ""
    say "Provisional components:"
    say "  $(c public) # Public storage for classroom sharing"
    say "  $(c secure) # Secure storage for host configuration"
    say ""
    say "Development components:"
    say "  $(c theme)         # Artwork for desktop images"
    say "  $(c void-packages) # Void Linux packages (TechLit's fork)"
    say "  $(c void-mklive)   # Void Linux ISO & Cloud scripts (TechLit's fork)"
    say "  $(c techlit-void)  # This code. Used internally to provision devices"
    say ""
    exit 1
  fi
}
