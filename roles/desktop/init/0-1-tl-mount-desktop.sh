#!/bin/sh

# Mount the host's persistent subvolume before initialization
if ! grep "/srv" "/etc/mtab" >"/dev/null"; then
  sdx="$(grep "/run/initramfs/live" "/etc/mtab" | sed -e 's/p\?. .*//')"
  mount "${sdx}4" -o "subvol=/@srv" "/srv"
fi
