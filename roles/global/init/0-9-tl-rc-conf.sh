#!/bin/sh

# Link and source host's rc.conf before other inintialization
if [ -r "/srv/secure/rc.conf" ]; then
  ln -sf "/srv/secure/rc.conf" "/etc/rc.conf"
  . "/etc/rc.conf"
fi
