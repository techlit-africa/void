#!/bin/sh

# Set hostname from techlit config
[ -n "$HOSTNAME" ] && echo "$HOSTNAME" > "/etc/hostname"
