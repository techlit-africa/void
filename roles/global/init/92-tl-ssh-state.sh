#!/bin/sh

mkdir -p "/home/admin/.ssh"

# Copy ssh identity for sshd
if [ -d "/srv/secure/ssh" ]; then
  install -o 0 -g 0 -m 600 -D "/srv/secure/ssh/"* "/etc/ssh/"
fi

# Copy rsa key for admin
if [ -f "/srv/secure/ssh/ssh_host_rsa_key" ]; then
  install -m 600 -D "/srv/secure/ssh/ssh_host_rsa_key" "/home/admin/.ssh/id_rsa"
fi

if [ -f "/srv/secure/ssh/ssh_host_rsa_key.pub" ]; then
  install -m 600 -D "/srv/secure/ssh/ssh_host_rsa_key.pub" "/home/admin/.ssh/id_rsa.pub"
fi

if [ -r "/srv/secure/known_hosts" ]; then
  ln -sf "/srv/secure/known_hosts" "/home/admin/.ssh/known_hosts"
fi

# Write authorized_keys file for admin user
if [ -n "$TL_ACCESS" ]; then
  echo "$TL_ACCESS" > "/home/admin/.ssh/authorized_keys"
fi

chown -R 1000:1000 "/home/admin/.ssh"
