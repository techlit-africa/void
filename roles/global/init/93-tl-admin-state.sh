#!/bin/sh

# Link admin bash history
[ -r "/srv/secure/bash_history" ] \
  && ln -sf "/srv/secure/bash_history" "/home/admin/.bash_history"
